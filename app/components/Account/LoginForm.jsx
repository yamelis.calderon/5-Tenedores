import React, { useState } from 'react';
import { StyleSheet,View } from 'react-native';
import {Input,Icon,Button} from 'react-native-elements';
import { isEmpty } from 'lodash';
import {validateEmail} from "../../utils/validations";
import{useNavigation } from "@react-navigation/native";
import * as firebase from "firebase";
import Loading from "../Loading";

export default function LoginForm(props) {
    const {toasRef } =props;
    const [showPassword,setShowPassword] = useState(false);
    const [formData, setFormData] = useState(defaultFormValue);
    const [loading, setLoading] = useState(false);
    const navigation = useNavigation();
    const onSubmit = () =>{

       if(isEmpty(formData.email) || isEmpty(formData.password))
       {
            toasRef.current.show("Todos los Campos son Obligatorios");
       }
       else if(!validateEmail(formData.email))
       {
            toasRef.current.show("Email no es valido");
       }else{
           setLoading(true);
           firebase.auth().signInWithEmailAndPassword(formData.email,formData.password)
           .then(() => {
               setLoading(false);
                navigation.navigate("account");
           })
           .catch(()=>{
               setLoading(false);
               toasRef.current.show("Email o contraseña incorrecto, porfavor verifique");
            });
       }
    }

    const onChange = (evento,type) =>{
        // console.log(evento.nativeEvent.text);
         setFormData({...formData, [type]:evento.nativeEvent.text});
     };
 
    return (
            <View style={styles.formContainer}>
                <Input
                    placeholder="Correo electronico"
                    containerStyle={styles.inputForm}
                    onChange = {(evento) => onChange(evento,"email")}
                    rightIcon={
                        <Icon
                        type="material-community" 
                        name="at" 
                        iconStyle={styles.iconRight}
                        /> 
                       }
                    
                />
                <Input
                    placeholder="Contraseña"
                    containerStyle={styles.inputForm}
                    secureTextEntry={showPassword ?  false : true}
                    onChange = {(evento) => onChange(evento,"password") }
                    rightIcon={
                        <Icon
                        type="material-community" 
                        name={showPassword ? "eye-off-outline" :"eye-outline"}  
                        iconStyle={styles.iconRight}
                        onPress={()=> setShowPassword(!showPassword)}
                        /> 
                       }
                    
                />
                <Button
                    title="Iniciar Sesión"
                    containerStyle={styles.btnContainerLogin}
                    buttonStyle={styles.btnLogin}
                    onPress={()=>onSubmit()}
                />              
                <Loading isVisible = {loading} text="Iniciando Sesion"/>
            </View>
        )
    
}

const defaultFormValue =() =>{
    return({email:"",password:""});
}

const styles = StyleSheet.create({
    formContainer:{
        flex:1,
        alignItems:"center",
        justifyContent:"center",
        marginTop:30,
    },
    inputForm:{
        width:"100%",
        marginTop:20,
    },
    btnContainerLogin:{
        marginTop:20,
        width:"95%",
    },
    btnLogin:{
        backgroundColor:"#00a680",
    },
    iconRight:{
        color:"#c1c1c1",
    },
})

