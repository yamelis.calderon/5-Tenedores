import React,{useRef} from "react";
import {StyleSheet,View, ScrollView, Text , Image } from "react-native";
import { Divider } from "react-native-elements";
import CreateAccount from "../auth/CreateAccount";
import Toast from "react-native-easy-toast";
import LoginForm from "../../../components/Account/LoginForm";

export default function Login() {
    const toasRef = useRef();
    return(
        <ScrollView>
            <Image 
                source={require("../../../../assets/img/5-tenedores.png")}
                resizeMode="contain"
                style = {styles.logo}
            />
            <View style={styles.viewContainer}> 
                <LoginForm toasRef={toasRef}/>
                <CreateAccount/>
            </View>
            <Divider style={styles.divider}/>
            <Text>Social Login </Text>
            <Toast ref={toasRef} position="center" opacity={0.9}/>
        </ScrollView>
    )
}

const styles = StyleSheet.create({
    logo:{
        width:"100%",
        height:150,
        marginTop:20,
    },
    viewContainer:{
        marginRight:40,
        marginLeft:40,
    },
    divider:{
        backgroundColor:"#00a680",
        margin:40,
    },

})