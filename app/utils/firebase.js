import firebase from "firebase/app";

const firebaseConfig = { 
     // Your web app's Firebase configuration
    apiKey: "AIzaSyDlWAZnUBGYLEFS-cYqn86e-x-YaKBhBFU",
    authDomain: "tenedores-9013b.firebaseapp.com",
    databaseURL: "https://tenedores-9013b.firebaseio.com",
    projectId: "tenedores-9013b",
    storageBucket: "tenedores-9013b.appspot.com",
    messagingSenderId: "178744460571",
    appId: "1:178744460571:web:9a5054b25ef74faaac7fbe"
};

export const firebaseApp = firebase.initializeApp(firebaseConfig);